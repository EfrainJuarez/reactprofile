import './componente1.css'
import profile from '../pictures/profile.jpeg';

function Ejemplo(){
    return (
        <>
        <div className="Container">
        <div  className="Card">
        <div className="photo-container">
    <img className="Photo" src={profile}/>
</div>
<div className="info">
            <h2>Efrain Juarez Mendoza</h2>
            <h3>Estudiante del ITC</h3>
            <p>Ingenieria en sistemas computacionales. 8vo Semestre</p>
        </div>
        </div>
        </div>

        <h1 id="title">Galeria</h1>

<div className="container">
    <div id="part1">
        <div className="photo">
                <div className="text">Perfil</div>
        </div>
        <div className="photo">
                <div className="text">Habilidades</div>
        </div>
        <div className="photo">
                <div className="text">Formacion</div>
        </div>
    </div>
    <div id="part2">
        <div className="photo"><div className="text">Cursos</div></div>
        <div className="photo"><div className="text">Profesores</div></div>
    </div>
</div>
        </>
    )
}

export default Ejemplo